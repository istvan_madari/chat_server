# Chat Server

### Introduction

The project provides a Telnet-based chat server implementation. The server accepts connections from Telnet clients 
and assigns unique names to each client. All the messages from the clients are time-stamped and stored on the server.
The server forwards the messages to all telnet clients. HTTP Clients are also allowed to query chat content (posting is
not implemented yet). 

### Requirements

To run the server you need Python3.

#### Ubuntu 18.04:
```sh
sudo apt install python3
```

#### OSX:
```sh
brew install python3
```

#### Additional packages
The server uses [netifaces](https://pypi.org/project/netifaces/) to determine the IP address of network interfaces. You can install it through pip3:
```sh
pip3 install netifaces
```

### Configuration
Before running, please check the `chatserver.ini` and adjust the network interface (`nic`) to your system.
E.g.: if your OS is connected to the network through network interface `enp0s8`, then please update the appropirate sections in the ini file:

```ini
[telnet]
nic = enp0s8
.
.
[http]
nic = enp0s8
.
.
```

### Run

Just simply run the `chat_server.py` from the project directory:

```sh
cd chat_server
python3 chat_server.py
```

To stop the server, just press any key in the terminal.

### Logging

Python logger is used for logging. The log messages are shown in the standard output and saved into file.
The logfile name and path can be configured in the `logger` section of the `chatserver.ini`:

```ini
[logger]
# Name of the log file. Can be absolute path.
filename = chatserver.log
```