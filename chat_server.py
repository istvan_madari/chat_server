#!/usr/bin/python3
'''Top level wrapper script for Chat Server.
Example:
    ``python3 chat_server.py``
'''

import run.main

if __name__ == '__main__':
    run.main.main()