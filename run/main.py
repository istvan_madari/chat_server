import os
from .server import ChatServer


def main():
    s = ChatServer()
    s.start()
    input('Press any key to exit...\n')
    s.terminate()
    print('Wating for background threads to stop')


if __name__ == '__main__':
    main()
    os._exit(0)