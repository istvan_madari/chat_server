import socket
import logging
import threading
import selectors
import types
import re
import configparser
import random
import netifaces as ni
import time

'''
Implements the chat server thread.
'''
class ChatServer(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.setDaemon(False)

        self.terminated = False
        self._lock = threading.Lock()
        self.messages = []
        self.selector = selectors.DefaultSelector()


        # Names of the connected clients
        self.clientNames = {}

        # Parsing the config file
        self.readconfig()

        # Default client names from file
        self.readnames()

        # Create the console logger
        console_handler = logging.StreamHandler()
        self.logger = logging.getLogger("chatserver")
        self.logger.setLevel(logging.INFO)
        self.logger.addHandler(console_handler)

        # Add file logger
        file_handler = logging.FileHandler(self.config['logger']['filename'])
        self.logger.addHandler(file_handler)

    '''
    Loads the possible client names from file.
    '''
    def readnames(self):
        with open(self.config['names']['filename'],'r') as namesfile:
            self.names = namesfile.readlines()

    '''
    Parses the config file. Returns (Err, ErrorMessage) if an exception happened.
    '''
    def readconfig(self):
        self.config = configparser.ConfigParser()
        config_name = 'chatserver.ini'

        try:
            self.config.read([config_name])
        except Exception as e:
            return True, f'System configuration file {config_name} has a problem: {str(e)}.'
        return False, ""

    '''
    Stops the server thread
    '''
    def terminate(self):
        self.logger.info("Terminating...")
        with self._lock:
            self.terminated = True

    def is_terminated(self):
        with self._lock:
            return self.terminated

    '''
    Checks if the HTTP request is a valid 'query' request
    '''
    def is_query(self, request):
        fields = request.split("\r\n")
        if fields:
            req_line = fields[0]
            # Just checking the first line now.
            return re.search("GET /query ", req_line) is not None
        return False

    '''
    Starts the server. Creates the Telnet and HTTP sockets, starts the message loop.
    '''
    def run(self):
        # Create a socket for the Telnet and a socket for the HTTP connections
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server_socket:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as http_socket:

                # If one of the server sockets couldn't be created, exit
                err, msg = self.init_telnet_socket(server_socket)
                if err:
                    self.logger.error(f"Couldn't initialize telnet service: {msg}")
                    return

                err, msg = self.init_http_socket(http_socket)
                if err:
                    self.logger.error(f"Couldn't initialize HTTP service: {msg}")
                    return

                self.loop(server_socket, http_socket)

    '''
    Message loop. Waits for incoming packages, forwards the message to the dispatcher.
    '''
    def loop(self, server_socket, http_socket):

        # Runs until terminate arrives from the main thread
        while not self.is_terminated():
            # Collects the ports where bytes arrived, or ready to be written
            events = self.selector.select(timeout=2)
            for key, mask in events:
                # Connection request on telnet port
                if key.data == "telnet":
                    self.handle_telnet_conn(server_socket)
                # Connection request on http port
                elif key.data == "http":
                    self.handle_http_conn(http_socket)
                # Message send or query
                else:
                    sock = key.fileobj
                    data = key.data

                    # Calls the dispatcher.
                    self.handle_messages(sock, data, mask)

    '''
    Initializes server socket for Telnet connections.
    :returns (True, ErrorMessage) if error happened, otherwise (False,_) 
    '''
    def init_telnet_socket(self, server_socket):
        try:
            err, telnet_host = self.get_nic_ip(self.config['telnet']['nic'])
            if err:
                return True, telnet_host
            telnet_port = int(self.config['telnet']['port'])
            server_socket.bind((telnet_host, telnet_port))
            server_socket.listen(1)
            self.logger.info(f'Telnet server was started on {(telnet_host, telnet_port)}')
            server_socket.setblocking(False)
            telnet_data = "telnet"
            self.selector.register(server_socket, selectors.EVENT_READ, data=telnet_data)
        except OSError as e:
            return True, e.strerror
        return False, ""

    '''
    Initializes server socket for the HTTP connections.
    :returns (True, ErrorMessage) if error happened, otherwise (False,_) 
    '''
    def init_http_socket(self, http_socket):
        try:
            err, http_host = self.get_nic_ip(self.config['http']['nic'])
            if err:
                return True, http_host

            http_port = int(self.config['http']['port'])

            http_socket.bind((http_host, http_port))
            http_socket.listen(1)
            self.logger.info(f'HTTP listener was started on {(http_host, http_port)}')
            http_socket.setblocking(False)
            http_data = "http"
            self.selector.register(http_socket, selectors.EVENT_READ, data=http_data)
        except OSError as e:
            return True, e.strerror
        return False, ""

    '''
    Accepts a new Telnet connection.
    Registers the new connection in the selector.
    '''
    def handle_telnet_conn(self, server_socket):
        conn, addr = server_socket.accept()
        self.logger.info(f'New connection: Teln - {addr}')
        conn.setblocking(False)

        # For each telnet connection the following details are saved:
        #  - Client IP address,
        #  - Protocol
        #  - Message pointer to the next message to be read
        #  - Default, randomly generated name
        client_name = self.generate_clientname()
        data = types.SimpleNamespace(addr=addr,
                                     type="telnet",
                                     msg_idx=0,
                                     client_name = client_name,
                                     system_message = f"Welcome! your name is {client_name}\n")
        self.selector.register(conn, selectors.EVENT_READ | selectors.EVENT_WRITE, data=data)

    '''
    Accepts a new Telnet connection.
    Registers the new connection in the selector.
    '''
    def handle_http_conn(self, http_socket):
        conn, addr = http_socket.accept()
        self.logger.info(f'New connection: Http - {addr}')
        conn.setblocking(False)

        # For each HTTP connection the following will be saved:
        #  - Client IP address,
        #  - Protocol
        #  - If there was a valid query on this connection
        data = types.SimpleNamespace(addr=addr, type="http", valid_query = None)
        self.selector.register(conn, selectors.EVENT_READ | selectors.EVENT_WRITE, data = data)

    '''
    Dispatch the request to the appropriate handler.
    '''
    def handle_messages(self, sock, data, mask):
        if data.type == 'telnet':
            self.handle_telnetmessage(sock, data, mask)
        elif data.type == 'http':
            self.handle_httpmessage(sock, data, mask)

    '''
    Handles a Telnet message
    '''
    def handle_telnetmessage(self, sock, data, mask):
        # There is a READ event on the socket
        if mask & selectors.EVENT_READ:
            # Read the message from the socket (byte array)
            recv_data = sock.recv(1024)
            if recv_data:
                # Saves the message into the buffer with client name and timestamp
                message = f'{data.client_name}: '.encode('utf-8') + recv_data
                message = f'[{time.strftime("%H:%M:%S")}] '.encode('utf-8') + message
                self.messages.append(message)
                data.msg_idx += 1
            else:
                # Disconnect
                self.logger.info(f'Closing connection: {data.addr}')
                del self.clientNames[data.client_name]
                self.selector.unregister(sock)
                sock.close()

        # The port is ready to be written
        if mask & selectors.EVENT_WRITE:
            # Write the system message first. E.g.: welcome message
            if data.system_message is not None:
                sock.send(data.system_message.encode("utf-8"))
                data.system_message = None

            # Continue to write from the last message
            while data.msg_idx < len(self.messages):
                sock.send(self.messages[data.msg_idx])
                data.msg_idx += 1

    '''
    Handles an HTTP message.
    Just the queries are handled.
    '''
    def handle_httpmessage(self, sock, data, mask):
        if mask & selectors.EVENT_READ:
            recv_data = sock.recv(1024)
            if recv_data:
                # If the URL is a valid query then, set the flag.
                # So, when the socket is ready the response is written
                is_valid = self.is_query(recv_data.decode('utf-8'))
                data.valid_query = is_valid
                if not is_valid:
                    self.logger.warning(f'Invalid HTTP request: {recv_data.decode("utf-8")}')
            else:
                self.selector.unregister(sock)
                sock.close()
        if mask & selectors.EVENT_WRITE:
            # The query was valid, write ALL the messages into the response
            if data.valid_query == True:
                if not self.messages:
                    sock.send(b"NO MESSAGES")
                else:
                    buffer = b"".join(self.messages)
                    sock.send(buffer)
                self.logger.warning(f'Replied to HTTP query, closing connection: {data.addr}')
                self.selector.unregister(sock)
                sock.close()
            # The query was not valid, disconnect
            elif data.valid_query == False:
                self.selector.unregister(sock)
                sock.close()

            # Otherwise it is None, it means that the port is ready for write, but the request hasn't been checked.

    '''
    Generates a unique name for the client and stores in a cache. 
    unique_name = randomname + {0..99}
    randomname is selected from the names file (configured in chatserver.ini)
    '''
    def generate_clientname(self):
        while True:
            name = self.names[random.randint(0, len(self.names)-1)].strip()
            name += str(random.randint(0,99))
            if name not in self.clientNames:
                self.clientNames[name] = True
                return name

    '''
    Returns the IP address of the given interface.
    '''
    def get_nic_ip(self, nic_name):
        try:
            ip = ni.ifaddresses(nic_name)[ni.AF_INET][0]['addr']
            return False, ip
        except ValueError as e:
            return True, f'Cannot determine IP address of nic "{nic_name}".'
